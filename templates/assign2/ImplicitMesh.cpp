//===========================================================================
/*
	CPSC 599.86 / 601.86 - Computer Haptics
	Winter 2018, University of Calgary

	This class encapsulates the visual and haptic rendering for an implicit
	surface.  It inherits the regular CHAI3D cMesh class, and taps into
	an external implementation of the Marching Cubes algorithm to create
	a triangle mesh from the implicit surface function.

	Your job is to implement the method that tracks the position of the
	proxy as the tool moves and interacts with the object, as described by
	the implicit surface rendering algorithm in Salisbury & Tarr 1997.

	\author    Modan Han
*/
//===========================================================================

#include "ImplicitMesh.h"
#include "MarchingSource.h"

using namespace chai3d;

double static_friction;
double dynamic_friction;

ImplicitMesh::ImplicitMesh()
	: m_surfaceFunction(0), m_projectedSphere(0.05)
{
	// because we are haptically rendering this object as an implicit surface
	// rather than a set of polygons, we will not need a collision detector
	setCollisionDetector(0);

	// create a sphere that tracks the position of the proxy
	m_projectedSphere.m_material->setWhite();
	addChild(&m_projectedSphere);
}

ImplicitMesh::~ImplicitMesh()
{
	// remove the proxy tracking sphere so that the base class doesn't delete it
	removeChild(&m_projectedSphere);
}

void ImplicitMesh::createFromFunction(double(*f)(double, double, double),
	cVector3d a_lowerBound, cVector3d a_upperBound,
	double a_granularity)
{
	// variables to hold raw triangles returned from marching cubes algorithm
	GLint tcount;
	GLfloat vertices[5 * 3 * 3];

	// sample the implicit surface by stepping through each dimension
	for (GLfloat x = a_lowerBound.x(); x <= a_upperBound.x(); x += a_granularity)
		for (GLfloat y = a_lowerBound.y(); y <= a_upperBound.y(); y += a_granularity)
			for (GLfloat z = a_lowerBound.z(); z <= a_upperBound.z(); z += a_granularity)
			{
				// call marching cubes to get the triangular facets for this cell
				vMarchCubeCustom(x, y, z, a_granularity, f, tcount, vertices);

				// add resulting triangles (if any) to our mesh
				for (int i = 0; i < tcount; ++i) {
					int ix = i * 9;
					this->newTriangle(
						cVector3d(vertices[ix + 0], vertices[ix + 1], vertices[ix + 2]),
						cVector3d(vertices[ix + 3], vertices[ix + 4], vertices[ix + 5]),
						cVector3d(vertices[ix + 6], vertices[ix + 7], vertices[ix + 8])
					);
				}
			}

	// compute face normals for our mesh so that lighting works properly
	this->computeAllNormals();

	// remember the function used to create this object
	m_surfaceFunction = f;
}

void ImplicitMesh::createFromFunction(double(*f)(double, double, double),
	double(*px)(double, double, double),
	double(*py)(double, double, double),
	double(*pz)(double, double, double),
	cVector3d a_lowerBound, cVector3d a_upperBound,
	double a_granularity) {
	createFromFunction(f, a_lowerBound, a_upperBound, a_granularity);
	m_surfaceFunctionPx = px;
	m_surfaceFunctionPy = py;
	m_surfaceFunctionPz = pz;
}

//! Contains code for graphically rendering this object in OpenGL.
void ImplicitMesh::render(cRenderOptions& a_options)
{
	// update the position and visibility of the proxy sphere
	m_projectedSphere.setShowEnabled(m_interactionInside);
	m_projectedSphere.setLocalPos(m_interactionPoint);

	// get the base class to render the mesh
	cMesh::render(a_options);
}


//===========================================================================
/*!
	This method should contain the core of the implicit surface rendering
	algorithm implementation.  The member variables m_interactionPoint
	and m_interactionInside should both be set by this method.

	\param  a_toolPos  Position of the tool.
	\param  a_toolVel  Velocity of the tool.
	\param  a_IDN  Identification number of the force algorithm.
*/
//===========================================================================
void ImplicitMesh::computeLocalInteraction(const cVector3d& a_toolPos,
	const cVector3d& a_toolVel,
	const unsigned int a_IDN)
{
	/////////////////////////////////////////////////////////////////////////
	// [CPSC.86] IMPLICIT SURFACE RENDERING ALGORITHM
	/////////////////////////////////////////////////////////////////////////

	// For Part II, the friction coefficients can be read from the object's
	// material as shown here.
	double mu_s = m_material->getStaticFriction();
	double mu_k = m_material->getDynamicFriction();

	m_functionValue = m_surfaceFunction(a_toolPos.x(), a_toolPos.y(), a_toolPos.z());
	m_debugVector = gradient(a_toolPos);

	if (in) {
		m_interactionInside = 1;
		computeLocalInteractionIn(a_toolPos, a_toolVel, a_IDN);
	}
	else {
		computeLocalInteractionOut(a_toolPos, a_toolVel, a_IDN);
		m_interactionInside = 0;
	}

}

void ImplicitMesh::computeLocalInteractionIn(const chai3d::cVector3d& a_toolPos,
	const chai3d::cVector3d& a_toolVel,
	const unsigned int a_IDN) {

	auto p = (a_toolPos)-planePos;
	// project p onto plane
	auto d = (p).dot(planeNormal);
	p -= d * planeNormal;
	p += planePos;

	// friction stuff
	if ((p - frictionPos).length() < static_friction) {

	}
	else {
		auto d = frictionPos - p;
		d.normalize();
		frictionPos = p + d * dynamic_friction;
	}
	p = frictionPos;

	m_interactionPoint = p;
	{
		auto dd = m_interactionPoint - a_toolPos;
		if (dd.length() > 1) { dd.normalize(); m_interactionPoint = a_toolPos + dd * 0.5; }
	}
	// holy dang, changed from computePlane(a_toolpos)
	// to this and everything worked! amazing
	computePlane(p);

	if (d > 0) {
		in = 0;
		m_interactionInside = 0;
	}
}

void ImplicitMesh::computeLocalInteractionOut(const chai3d::cVector3d& a_toolPos,
	const chai3d::cVector3d& a_toolVel,
	const unsigned int a_IDN) {

	if (m_functionValue < 0) {
		in = 1;
		computePlane(a_toolPos);
		frictionPos = planePos;
	}
}

void ImplicitMesh::computePlane(chai3d::cVector3d p) {
	auto c = closest(p);
	auto g = gradient(c);
	planePos = c;
	g.normalize();
	planeNormal = g;
}

chai3d::cVector3d ImplicitMesh::gradient(chai3d::cVector3d p) {
	double dx, dy, dz;
	dx = m_surfaceFunctionPx(p.x(), p.y(), p.z());
	dy = m_surfaceFunctionPy(p.x(), p.y(), p.z());
	dz = m_surfaceFunctionPz(p.x(), p.y(), p.z());
	cVector3d d(dx, dy, dz);
	return d;
}

chai3d::cVector3d ImplicitMesh::closest(chai3d::cVector3d p) {
	int m = 0;
	cVector3d dp;
	do {
		auto d = gradient(p);
		dp = -m_surfaceFunction(p.x(), p.y(), p.z()) * d / d.lengthsq();
		p = p + dp;
		//	auto functionValue = m_surfaceFunction(p.x(), p.y(), p.z());
	} while (dp.length() > 0.001);
	return p;
}