//===========================================================================
/*
    CPSC 599.86 / 601.86 - Computer Haptics
    Winter 2018, University of Calgary

    This class encapsulates the visual and haptic rendering for an implicit
    surface.  It inherits the regular CHAI3D cMesh class, and taps into
    an external implementation of the Marching Cubes algorithm to create
    a triangle mesh from the implicit surface function.

    Your job is to implement the method that tracks the position of the
    proxy as the tool moves and interacts with the object, as described by
    the implicit surface rendering algorithm in Salisbury & Tarr 1997.

    \author    Modan Han
*/
//===========================================================================

#ifndef IMPLICITMESH_H
#define IMPLICITMESH_H

#include "chai3d.h"

extern double static_friction;
extern double dynamic_friction;

class ImplicitMesh : public chai3d::cMesh
{
    //! A visible sphere that tracks the position of the proxy on the surface
    chai3d::cShapeSphere m_projectedSphere;
    
    //! A pointer to the implicit function used to create this object
    double (*m_surfaceFunction)(double, double, double);

	double(*m_surfaceFunctionPx)(double, double, double);
	double(*m_surfaceFunctionPy)(double, double, double);
	double(*m_surfaceFunctionPz)(double, double, double);

	bool in;
	chai3d::cVector3d planePos;
	chai3d::cVector3d planeNormal;
	chai3d::cVector3d frictionPos;

	chai3d::cVector3d gradient(chai3d::cVector3d p);
	chai3d::cVector3d closest(chai3d::cVector3d p);

	void computePlane(chai3d::cVector3d p);

	void computeLocalInteractionIn(const chai3d::cVector3d& a_toolPos,
		const chai3d::cVector3d& a_toolVel,
		const unsigned int a_IDN);
	void computeLocalInteractionOut(const chai3d::cVector3d& a_toolPos,
		const chai3d::cVector3d& a_toolVel,
		const unsigned int a_IDN);
    
public:
    ImplicitMesh();
    virtual ~ImplicitMesh();

    //! Create a polygon mesh from an implicit surface function for visual rendering.
    void createFromFunction(double (*f)(double, double, double),
                            chai3d::cVector3d a_lowerBound,
                            chai3d::cVector3d a_upperBound,
                            double a_granularity);

	//! Create a polygon mesh from an implicit surface function for visual rendering.
	void createFromFunction(double(*f)(double, double, double),
		double(*px)(double,double,double),
		double(*py)(double, double, double),
		double(*pz)(double, double, double),
		chai3d::cVector3d a_lowerBound,
		chai3d::cVector3d a_upperBound,
		double a_granularity);

    //! Contains code for graphically rendering this object in OpenGL.
    virtual void render(chai3d::cRenderOptions& a_options);

    //! Update the geometric relationship between the tool and the current object.
    virtual void computeLocalInteraction(const chai3d::cVector3d& a_toolPos,
                                         const chai3d::cVector3d& a_toolVel,
                                         const unsigned int a_IDN);

chai3d::cVector3d m_debugVector;
double m_functionValue;
};

#endif
