//==============================================================================
/*
	CPSC 599.86 / 601.86 - Computer Haptics
	Winter 2018, University of Calgary

	This class extends the cAlgorithmFingerProxy class in CHAI3D that
	implements the god-object/finger-proxy haptic rendering algorithm.
	It allows us to modify or recompute the force that is ultimately sent
	to the haptic device.

	Your job for this assignment is to implement the updateForce() method
	in this class to support for two new effects: force shading and haptic
	textures. Methods for both are described in Ho et al. 1999.
*/
//==============================================================================

#include "MyProxyAlgorithm.h"
#include "MyMaterial.h"

using namespace chai3d;

//==============================================================================
/*!
	This method uses the information computed earlier in
	computeNextBestProxyPosition() to calculate the force to be rendered.
	It first calls cAlgorithmFingerProxy::updateForce() to compute the base
	force from contact geometry and the constrained proxy position. That
	force can then be modified or recomputed in this function.

	Your implementation of haptic texture mapping will likely end up in this
	function. When this function is called, collision detection has already
	been performed, and the proxy point has already been updated based on the
	constraints found. Your job is to compute a force with all that information
	available to you.

	Useful variables to read:
		m_deviceGlobalPos   - current position of haptic device
		m_proxyGlboalPos    - computed position of the constrained proxy
		m_numCollisionEvents- the number of surfaces constraining the proxy
		m_collisionRecorderConstraint0,1,2
							- up to three cCollisionRecorder structures with
							  cCollisionEvents that contain very useful
							  information about each contact

	Variables that this function should set/reset:
		m_normalForce       - computed force applied in the normal direction
		m_tangentialForce   - computed force along the tangent of the surface
		m_lastGlobalForce   - this is what the operator ultimately feels!!!
*/
//==============================================================================


// quite a terrible idea using a global variable ._.
bool kinetic_friction;
cVector3d uv;

cMatrix3d getBTNAtPosition(const cTriangleArrayPtr cta, const unsigned int a_triangleIndex,
	const cVector3d& a_localPos)
{
	// get vertices of contact triangles
	cVector3d vertex0 = cta->m_vertices->getLocalPos(cta->getVertexIndex0(a_triangleIndex));
	cVector3d vertex1 = cta->m_vertices->getLocalPos(cta->getVertexIndex1(a_triangleIndex));
	cVector3d vertex2 = cta->m_vertices->getLocalPos(cta->getVertexIndex2(a_triangleIndex));

	// project desired point on triangle
	cVector3d vertex = cProjectPointOnTriangle(a_localPos, vertex0, vertex1, vertex2);

	// compute area of triangle
	double area = cTriangleArea(vertex0, vertex1, vertex2);

	// compute areas of three sub-triangles formed by the three vertices of the triangle and the contact point.
	double area0 = cTriangleArea(vertex, vertex1, vertex2);
	double area1 = cTriangleArea(vertex, vertex0, vertex2);
	double area2 = cTriangleArea(vertex, vertex0, vertex1);

	// compute weights based on position of contact point
	double c0, c1, c2;
	if (area > 0.0)
	{
		c0 = area0 / area;
		c1 = area1 / area;
		c2 = area2 / area;
	}
	else
	{
		c0 = c1 = c2 = (1.0 / 3.0);
	}

	cVector3d b, t, n;


	// retrieve the texture coordinate for each triangle vertex
	cVector3d b0 = cta->m_vertices->getBitangent(cta->getVertexIndex0(a_triangleIndex));
	cVector3d b1 = cta->m_vertices->getBitangent(cta->getVertexIndex1(a_triangleIndex));
	cVector3d b2 = cta->m_vertices->getBitangent(cta->getVertexIndex2(a_triangleIndex));

	cVector3d t0 = cta->m_vertices->getTangent(cta->getVertexIndex0(a_triangleIndex));
	cVector3d t1 = cta->m_vertices->getTangent(cta->getVertexIndex1(a_triangleIndex));
	cVector3d t2 = cta->m_vertices->getTangent(cta->getVertexIndex2(a_triangleIndex));

	cVector3d n0 = cta->m_vertices->getNormal(cta->getVertexIndex0(a_triangleIndex));
	cVector3d n1 = cta->m_vertices->getNormal(cta->getVertexIndex1(a_triangleIndex));
	cVector3d n2 = cta->m_vertices->getNormal(cta->getVertexIndex2(a_triangleIndex));

	// compute the exact texture coordinate at the contact point
	b = cAdd(cMul(c0, b0), cMul(c1, b1), cMul(c2, b2));
	t = cAdd(cMul(c0, t0), cMul(c1, t1), cMul(c2, t2));
	n = cAdd(cMul(c0, n0), cMul(c1, n1), cMul(c2, n2));


	// return result
	cMatrix3d ans(b, t, n);
	return ans;
}

void MyProxyAlgorithm::updateForce()
{
	// get the base class to do basic force computation first
	cAlgorithmFingerProxy::updateForce();

	// TODO: compute force shading and texture forces here

	if (m_numCollisionEvents > 0)
	{
		// this is how you access collision information from the first constraint
		cCollisionEvent* c0 = &m_collisionRecorderConstraint0.m_nearestCollision;

		if (MyMaterialPtr material = std::dynamic_pointer_cast<MyMaterial>(c0->m_object->m_material))
		{
			// you can access your custom material properties here

			auto t = c0->m_triangles;
			uv = t->getTexCoordAtPosition(c0->m_index, m_proxyGlobalPos - c0->m_object->getGlobalPos());
			auto d = m_normalForce.length();

			auto transformation = getBTNAtPosition(c0->m_triangles, c0->m_index, m_proxyGlobalPos - c0->m_object->getGlobalPos());
			cVector3d finalNormal;

			if (material->m_bump) {
				// special case bump
				finalNormal = cVector3d(0, sin(uv.x() * M_PI * 20), 1);
				finalNormal.normalize();
				finalNormal *= d;
				finalNormal = transformation * finalNormal;

				m_normalForce = finalNormal;
				m_lastGlobalForce = m_normalForce + m_tangentialForce;
			}
			else if (material->m_fric) {

			}
			else {
				if (material->m_myNormalMap) {
					cColorf color;
					auto image = material->m_myNormalMap->m_image;
					auto u = fmod(uv.x() *image->getWidth(), image->getWidth());
					auto v = fmod(uv.y()* image->getHeight(), image->getHeight());
					if (u < 0)u += image->getWidth();
					if (v < 0)v += image->getHeight();
					if (image->getPixelColorInterpolated(u, v, color)) {
						finalNormal.y(-color.getR() + 0.5);
						finalNormal.x(-color.getG() + 0.5);
						finalNormal.z(color.getB() / 2);
						finalNormal.normalize();
						finalNormal *= d;
						finalNormal = transformation * finalNormal;
						m_normalForce = finalNormal;
						m_lastGlobalForce = m_normalForce + m_tangentialForce;
					}
					else {
						std::cout << "asdf" << std::endl;
					}

				}
			}
		}
	}
}


//==============================================================================
/*!
	This method attempts to move the proxy, subject to friction constraints.
	This is called from computeNextBestProxyPosition() when the proxy is
	ready to move along a known surface.

	Your implementation of friction mapping will likely need to modify or
	replace the CHAI3D implementation in cAlgorithmFingerProxy. You may
	either copy the implementation from the base class and modify it to take
	into account a friction map, or use your own friction rendering from your
	previous assignment.

	The most important thing to do in this method is to write the desired
	proxy position into the m_nextBestProxyGlobalPos member variable.

	The input parameters to this function are as follows, all provided in the
	world (global) coordinate frame:

	\param  a_goal    The location to which we'd like to move the proxy.
	\param  a_proxy   The current position of the proxy.
	\param  a_normal  The surface normal at the obstructing surface.
	\param  a_parent  The surface along which we're moving.
*/
//==============================================================================
void MyProxyAlgorithm::testFrictionAndMoveProxy(const cVector3d& a_goal,
	const cVector3d& a_proxy,
	cVector3d &a_normal,
	cGenericObject* a_parent)
{
	if (m_numCollisionEvents > 0)
	{
		cCollisionEvent* c0 = &m_collisionRecorderConstraint0.m_nearestCollision;
		cAlgorithmFingerProxy::testFrictionAndMoveProxy(a_goal, a_proxy, a_normal, a_parent);

		float sf = 0.000, kf = 0.000;

		if (MyMaterialPtr material = std::dynamic_pointer_cast<MyMaterial>(c0->m_object->m_material))
			if (material->m_fric) {
				sf = 0.0015 * std::max(sin(uv.y() * M_PI * 10) + 1, 0.0);
				kf = sf * 0.75;
			}
			else if (material->m_myRoughnessMap) {
				auto image = material->m_myRoughnessMap->m_image;
				auto u = fmod(uv.x() *image->getWidth(), image->getWidth());
				auto v = fmod(uv.y()* image->getHeight(), image->getHeight());
				if (u < 0)u += image->getWidth();
				if (v < 0)v += image->getHeight();
				cColorf color;
				if (image->getPixelColorInterpolated(u, v, color)) {
					sf = (color.getR())*0.003;
					kf = sf * 0.75;
				}
				else {
					std::cout << "asdf" << std::endl;
				}
			}


			auto d = a_proxy - a_goal;
			if (kinetic_friction) {
				if (d.length() > kf) {
					auto td = d; td.normalize();
					m_nextBestProxyGlobalPos = a_goal + td * kf * 1.01;
				}
				else {
					m_nextBestProxyGlobalPos = a_proxy;
					kinetic_friction = 0;
				}
			}
			else {
				if (d.length() > sf) {
					auto td = d; td.normalize();
					m_nextBestProxyGlobalPos = a_goal + td * kf * 1.01;
					kinetic_friction = 1;
				}
				else {
					m_nextBestProxyGlobalPos = a_proxy;
				}
			}
	}
}
