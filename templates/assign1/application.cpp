//==============================================================================
/*
	\author    Modan Han
*/
//==============================================================================

//------------------------------------------------------------------------------
#include "chai3d.h"
//------------------------------------------------------------------------------
#include <GLFW/glfw3.h>
#include <cmath>
//------------------------------------------------------------------------------
using namespace chai3d;
using namespace std;
//------------------------------------------------------------------------------

int mode = 0;

cMesh* sphere;
cMesh* cube;

double sphere_radius = 0.02;
double cube_size = 0.05;
double cube_z = -0.035;
double stiffness = 2000;

cMesh* magCylinder;
double mag_length = 0.05;
cMesh* magSphere;

double mag_range = 0.0375;
double mag_force = 100;

void UpdateMode();




//------------------------------------------------------------------------------
// GENERAL SETTINGS
//------------------------------------------------------------------------------

// stereo Mode
/*
	C_STEREO_DISABLED:            Stereo is disabled
	C_STEREO_ACTIVE:              Active stereo for OpenGL NVDIA QUADRO cards
	C_STEREO_PASSIVE_LEFT_RIGHT:  Passive stereo where L/R images are rendered next to each other
	C_STEREO_PASSIVE_TOP_BOTTOM:  Passive stereo where L/R images are rendered above each other
*/
cStereoMode stereoMode = C_STEREO_DISABLED;

// fullscreen mode
bool fullscreen = false;

// mirrored display
bool mirroredDisplay = false;


//------------------------------------------------------------------------------
// DECLARED VARIABLES
//------------------------------------------------------------------------------

// a world that contains all objects of the virtual environment
cWorld* world;

// a camera to render the world in the window display
cCamera* camera;

// a light source to illuminate the objects in the world
cDirectionalLight *light;

// a haptic device handler
cHapticDeviceHandler* handler;

// a pointer to the current haptic device
cGenericHapticDevicePtr hapticDevice;

// a label to display the rates [Hz] at which the simulation is running
cLabel* labelRates;

// a small sphere (cursor) representing the haptic device 
cShapeSphere* cursor;

// flag to indicate if the haptic simulation currently running
bool simulationRunning = false;

// flag to indicate if the haptic simulation has terminated
bool simulationFinished = false;

// a frequency counter to measure the simulation graphic rate
cFrequencyCounter freqCounterGraphics;

// a frequency counter to measure the simulation haptic rate
cFrequencyCounter freqCounterHaptics;

// haptic thread
cThread* hapticsThread;

// a handle to window display context
GLFWwindow* window = NULL;

// current width of window
int width = 0;

// current height of window
int height = 0;

// swap interval for the display context (vertical synchronization)
int swapInterval = 1;


//------------------------------------------------------------------------------
// DECLARED FUNCTIONS
//------------------------------------------------------------------------------

// callback when the window display is resized
void windowSizeCallback(GLFWwindow* a_window, int a_width, int a_height);

// callback when an error GLFW occurs
void errorCallback(int error, const char* a_description);

// callback when a key is pressed
void keyCallback(GLFWwindow* a_window, int a_key, int a_scancode, int a_action, int a_mods);

// this function renders the scene
void updateGraphics(void);

// this function contains the main haptics simulation loop
void updateHaptics(void);

// this function closes the application
void close(void);


//==============================================================================
/*
	TEMPLATE:    application.cpp

	Description of your application.
*/
//==============================================================================

int main(int argc, char* argv[])
{
	//--------------------------------------------------------------------------
	// INITIALIZATION
	//--------------------------------------------------------------------------

	cout << endl;
	cout << "-----------------------------------" << endl;
	cout << "CHAI3D" << endl;
	cout << "-----------------------------------" << endl << endl << endl;
	cout << "Keyboard Options:" << endl << endl;
	cout << "[f] - Enable/Disable full screen mode" << endl;
	cout << "[m] - Enable/Disable vertical mirroring" << endl;
	cout << "[q] - Exit application" << endl;
	cout << "[Right arrow] - Next scene" << endl;
	cout << "-----------------------------------" << endl << endl << endl;
	cout << "Scene 1: Sphere and Box" << endl;
	cout << "Scene 2: Magnetic Fields" << endl;
	cout << "Scene 3: Animated Magnetic Fields (position and point magnet strength)" << endl;
	cout << "Scene 4: Animated Sphere and Box (cube position)" << endl;
	cout << endl << endl;


	//--------------------------------------------------------------------------
	// OPENGL - WINDOW DISPLAY
	//--------------------------------------------------------------------------

	// initialize GLFW library
	if (!glfwInit())
	{
		cout << "failed initialization" << endl;
		cSleepMs(1000);
		return 1;
	}

	// set error callback
	glfwSetErrorCallback(errorCallback);

	// compute desired size of window
	const GLFWvidmode* mode = glfwGetVideoMode(glfwGetPrimaryMonitor());
	int w = 0.8 * mode->height;
	int h = 0.5 * mode->height;
	int x = 0.5 * (mode->width - w);
	int y = 0.5 * (mode->height - h);

	// set OpenGL version
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 2);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 1);

	// set active stereo mode
	if (stereoMode == C_STEREO_ACTIVE)
	{
		glfwWindowHint(GLFW_STEREO, GL_TRUE);
	}
	else
	{
		glfwWindowHint(GLFW_STEREO, GL_FALSE);
	}

	// create display context
	window = glfwCreateWindow(w, h, "CHAI3D", NULL, NULL);
	if (!window)
	{
		cout << "failed to create window" << endl;
		cSleepMs(1000);
		glfwTerminate();
		return 1;
	}

	// get width and height of window
	glfwGetWindowSize(window, &width, &height);

	// set position of window
	glfwSetWindowPos(window, x, y);

	// set key callback
	glfwSetKeyCallback(window, keyCallback);

	// set resize callback
	glfwSetWindowSizeCallback(window, windowSizeCallback);

	// set current display context
	glfwMakeContextCurrent(window);

	// sets the swap interval for the current display context
	glfwSwapInterval(swapInterval);

#ifdef GLEW_VERSION
	// initialize GLEW library
	if (glewInit() != GLEW_OK)
	{
		cout << "failed to initialize GLEW library" << endl;
		glfwTerminate();
		return 1;
	}
#endif


	//--------------------------------------------------------------------------
	// WORLD - CAMERA - LIGHTING
	//--------------------------------------------------------------------------

	// create a new world.
	world = new cWorld();

	// set the background color of the environment
	world->m_backgroundColor.setBlack();

	// create a camera and insert it into the virtual world
	camera = new cCamera(world);
	world->addChild(camera);

	// position and orient the camera
	camera->set(cVector3d(0.125, 0.0, 0.05),    // camera position (eye)
		cVector3d(0.0, 0.0, -0.01),    // look at position (target)
		cVector3d(0.0, 0.0, 1.0));   // direction of the (up) vector

// set the near and far clipping planes of the camera
	camera->setClippingPlanes(0.01, 10.0);

	// set stereo mode
	camera->setStereoMode(stereoMode);

	// set stereo eye separation and focal length (applies only if stereo is enabled)
	camera->setStereoEyeSeparation(0.01);
	camera->setStereoFocalLength(0.5);

	// set vertical mirrored display mode
	camera->setMirrorVertical(mirroredDisplay);

	// create a directional light source
	light = new cDirectionalLight(world);

	// insert light source inside world
	world->addChild(light);

	// enable light source
	light->setEnabled(true);

	// define direction of light beam
	light->setDir(-1.0, 0.0, 0.0);

	// create a sphere (cursor) to represent the haptic device
	cursor = new cShapeSphere(0.01);

	// insert cursor inside world
	world->addChild(cursor);


	sphere = new cMesh();
	cCreateSphere(sphere, sphere_radius);
	world->addChild(sphere);


	cube = new cMesh();
	cCreateBox(cube, cube_size, cube_size, cube_size);
	world->addChild(cube);
	cube->setLocalPos(cVector3d(0.00, 0.00, cube_z));

	magCylinder = new cMesh();
	cCreateCylinder(magCylinder, mag_length, 0.001);
	world->addChild(magCylinder);
	auto magSphere0 = new cMesh();
	cCreateSphere(magSphere0, 0.001);
	auto magSphere1 = new cMesh();
	cCreateSphere(magSphere1, 0.001);
	magSphere1->setLocalPos(cVector3d(0, 0, mag_length));
	magCylinder->addChild(magSphere0);
	magCylinder->addChild(magSphere1);

	magSphere = new cMesh();
	cCreateSphere(magSphere, 0.001);
	world->addChild(magSphere);


	UpdateMode();



	//--------------------------------------------------------------------------
	// HAPTIC DEVICE
	//--------------------------------------------------------------------------

	// create a haptic device handler
	handler = new cHapticDeviceHandler();

	// get a handle to the first haptic device
	handler->getDevice(hapticDevice, 0);

	// open a connection to haptic device
	hapticDevice->open();

	// calibrate device (if necessary)
	hapticDevice->calibrate();

	// retrieve information about the current haptic device
	cHapticDeviceInfo info = hapticDevice->getSpecifications();

	// display a reference frame if haptic device supports orientations
	if (info.m_sensedRotation == true)
	{
		// display reference frame
		cursor->setShowFrame(true);

		// set the size of the reference frame
		cursor->setFrameSize(0.05);
	}

	// if the device has a gripper, enable the gripper to simulate a user switch
	hapticDevice->setEnableGripperUserSwitch(true);


	//--------------------------------------------------------------------------
	// WIDGETS
	//--------------------------------------------------------------------------

	// create a font
	cFontPtr font = NEW_CFONTCALIBRI20();

	// create a label to display the haptic and graphic rates of the simulation
	labelRates = new cLabel(font);
	labelRates->m_fontColor.setWhite();
	camera->m_frontLayer->addChild(labelRates);


	//--------------------------------------------------------------------------
	// START SIMULATION
	//--------------------------------------------------------------------------

	// create a thread which starts the main haptics rendering loop
	hapticsThread = new cThread();
	hapticsThread->start(updateHaptics, CTHREAD_PRIORITY_HAPTICS);

	// setup callback when application exits
	atexit(close);


	//--------------------------------------------------------------------------
	// MAIN GRAPHIC LOOP
	//--------------------------------------------------------------------------

	// call window size callback at initialization
	windowSizeCallback(window, width, height);

	// main graphic loop
	while (!glfwWindowShouldClose(window))
	{
		// get width and height of window
		glfwGetWindowSize(window, &width, &height);

		// render graphics
		updateGraphics();

		// swap buffers
		glfwSwapBuffers(window);

		// process events
		glfwPollEvents();

		// signal frequency counter
		freqCounterGraphics.signal(1);
	}

	// close window
	glfwDestroyWindow(window);

	// terminate GLFW library
	glfwTerminate();

	// exit
	return 0;
}

//------------------------------------------------------------------------------

void windowSizeCallback(GLFWwindow* a_window, int a_width, int a_height)
{
	// update window size
	width = a_width;
	height = a_height;
}

//------------------------------------------------------------------------------

void errorCallback(int a_error, const char* a_description)
{
	cout << "Error: " << a_description << endl;
}

//------------------------------------------------------------------------------

void keyCallback(GLFWwindow* a_window, int a_key, int a_scancode, int a_action, int a_mods)
{
	// filter calls that only include a key press
	if (a_action != GLFW_PRESS)
	{
		return;
	}

	// option - exit
	else if ((a_key == GLFW_KEY_ESCAPE) || (a_key == GLFW_KEY_Q))
	{
		glfwSetWindowShouldClose(a_window, GLFW_TRUE);
	}

	// option - toggle fullscreen
	else if (a_key == GLFW_KEY_F)
	{
		// toggle state variable
		fullscreen = !fullscreen;

		// get handle to monitor
		GLFWmonitor* monitor = glfwGetPrimaryMonitor();

		// get information about monitor
		const GLFWvidmode* mode = glfwGetVideoMode(monitor);

		// set fullscreen or window mode
		if (fullscreen)
		{
			glfwSetWindowMonitor(window, monitor, 0, 0, mode->width, mode->height, mode->refreshRate);
			glfwSwapInterval(swapInterval);
		}
		else
		{
			int w = 0.8 * mode->height;
			int h = 0.5 * mode->height;
			int x = 0.5 * (mode->width - w);
			int y = 0.5 * (mode->height - h);
			glfwSetWindowMonitor(window, NULL, x, y, w, h, mode->refreshRate);
			glfwSwapInterval(swapInterval);
		}
	}

	// option - toggle vertical mirroring
	else if (a_key == GLFW_KEY_M)
	{
		mirroredDisplay = !mirroredDisplay;
		camera->setMirrorVertical(mirroredDisplay);
	}

	else if (a_key == GLFW_KEY_RIGHT) {
		mode = (mode + 1) % 4;
		UpdateMode();
	}
}

void UpdateMode() {
	sphere->setEnabled(mode == 0 || mode==3);
	cube->setEnabled(mode == 0||mode==3);
	magCylinder->setEnabled(mode == 1 || mode==2, true);
	magSphere->setEnabled(mode == 1||mode ==2, true);
}

//------------------------------------------------------------------------------

void close(void)
{
	// stop the simulation
	simulationRunning = false;

	// wait for graphics and haptics loops to terminate
	while (!simulationFinished) { cSleepMs(100); }

	// close haptic device
	hapticDevice->close();

	// delete resources
	delete hapticsThread;
	delete world;
	delete handler;
}

//------------------------------------------------------------------------------

void updateGraphics(void)
{
	/////////////////////////////////////////////////////////////////////
	// UPDATE WIDGETS
	/////////////////////////////////////////////////////////////////////

	// update haptic and graphic rate data
	labelRates->setText(cStr(freqCounterGraphics.getFrequency(), 0) + " Hz / " +
		cStr(freqCounterHaptics.getFrequency(), 0) + " Hz");

	// update position of label
	labelRates->setLocalPos((int)(0.5 * (width - labelRates->getWidth())), 15);


	/////////////////////////////////////////////////////////////////////
	// RENDER SCENE
	/////////////////////////////////////////////////////////////////////

	// update shadow maps (if any)
	world->updateShadowMaps(false, mirroredDisplay);

	// render world
	camera->renderView(width, height);

	// wait until all GL commands are completed
	glFinish();

	// check for any OpenGL errors
	GLenum err;
	err = glGetError();
	if (err != GL_NO_ERROR) cout << "Error:  %s\n" << gluErrorString(err);
}

//------------------------------------------------------------------------------

void updateHaptics(void)
{
	// simulation in now running
	simulationRunning = true;
	simulationFinished = false;

	cPrecisionClock timer;
	timer.start();

	// main haptic simulation loop
	while (simulationRunning)
	{
		double time = timer.getCurrentTimeSeconds();

		/////////////////////////////////////////////////////////////////////
		// READ HAPTIC DEVICE
		/////////////////////////////////////////////////////////////////////

		// read position 
		cVector3d position;
		hapticDevice->getPosition(position);

		// read orientation 
		cMatrix3d rotation;
		hapticDevice->getRotation(rotation);

		// read user-switch status (button 0)
		bool button = false;
		hapticDevice->getUserSwitch(0, button);


		/////////////////////////////////////////////////////////////////////
		// UPDATE 3D CURSOR MODEL
		/////////////////////////////////////////////////////////////////////

		// update position and orienation of cursor
		cursor->setLocalPos(position);
		cursor->setLocalRot(rotation);

		/////////////////////////////////////////////////////////////////////
		// COMPUTE FORCES
		/////////////////////////////////////////////////////////////////////

		cVector3d force(0, 0, 0);
		cVector3d torque(0, 0, 0);
		double gripperForce = 0.0;


		stiffness = 1500 + cos(time*1000) * 1500;

		if (mode == 0 || mode == 3) {
			double radius = 0.03;
			auto ds = position - cVector3d(0.00, 0.00, 0.00);

			if (mode == 3) {
				cube_z = -0.03 + cos(time)*0.02;
			}
			else{
				cube_z = -0.035;
			}
			cube->setLocalPos(0, 0, cube_z);

			cVector3d ps;
			bool inSphere;
			{
				ps = position;
				ps.normalize();
				ps *= sphere_radius;
				inSphere = position.length() <= sphere_radius;
			}

			bool inCube;
			cVector3d pc, position_c;
			{
				position_c = position + cVector3d(0, 0, -cube_z);
				double cs = cube_size / 2;
				if (abs(abs(position_c.x()) - cs) < abs(abs(position_c.y()) - cs) && abs(abs(position_c.x()) - cs) < abs(abs(position_c.z()) - cs)) {
					pc.set(cs * ((position_c.x() < 0) ? -1 : 1), position_c.y(), position_c.z());
				}
				else if (abs(abs(position_c.y()) - cs) < abs(abs(position_c.z()) - cs)) {
					pc.set(position_c.x(), cs * ((position_c.y() < 0) ? -1 : 1), position_c.z());
				}
				else {
					pc.set(position_c.x(), position_c.y(), cs  * ((position_c.z() < 0) ? -1 : 1));
				}
				pc += cVector3d(0, 0, cube_z);
				inCube = abs(position_c.x()) <= cs && abs(position_c.y()) <= cs && abs(position_c.z()) <= cs;
			}

			cVector3d pi;
			{
				double l = cube_size / 2 + cube_z;
				double r = sphere_radius * sphere_radius - l * l;
				r = sqrt(r);
				pi = position;
				pi.set(pi.x(), pi.y(), 0);
				pi.normalize();
				pi *= r;
				pi.set(pi.x(), pi.y(), l);
			}

			auto ans = pi;
			{
				bool inside = 0;
				{
					auto tps = ps + cVector3d(0, 0, -cube_z);
					bool psinCube = abs(tps.x()) <= cube_size / 2 && abs(tps.y()) <= cube_size / 2 && abs(tps.z()) <= cube_size / 2;
					if (inSphere&&psinCube) { inside = 1; }
				}
				{
					auto tpc = pc;
					if (inCube&&tpc.length() <= sphere_radius) { inside = 1; }
				}
				if (!inside) {
					if (inCube)ans = pc;
					if (inSphere)ans = ps;
				}
			}


			if (inCube || inSphere)
			{
				auto f = ans - position;
				force.set(f.x()*stiffness, f.y()*stiffness, f.z()*stiffness);
			}
		}
		else if (mode == 1 || mode == 2) {
			force.set(0, 0, 0);

			if (mode==2) {
				magCylinder->setLocalPos(cVector3d(cos(time / 2)*0.01, sin(time / 2)*0.01 - 0.0125, -0.05));
				magSphere->setLocalPos(0, 0.025, cos(time)*0.025);
			}
			else {
				magCylinder->setLocalPos(cVector3d(0, - 0.0125, -0.025));
				magSphere->setLocalPos(0, 0.025, cos(0)*0.010);
			}


			double tf = 750 + 750 * cos(time);
			if (mode == 1)tf = 1000;

			auto cBegin = magCylinder->getLocalPos();
			auto cEnd = cBegin + cVector3d(0, 0, mag_length);
			auto cPoint = magSphere->getLocalPos();

			if ((cPoint - position).length() < mag_range) {
				auto f = (cPoint - position);
				auto l = f.length();
				// wow amazing hack v2.0
				double k= tf*l*exp(-l*l * 5000);
				f.normalize();
				f *= k;
				force += f;
			}
			auto closest_point=cBegin;
			{
				auto line = cEnd - cBegin;
				auto proj = cProject(position-cBegin, line);
				if (proj.length() > line.length()) {
					closest_point = cEnd;
				}
				else if ((proj.x() > 0 && line.x() < 0)|| (proj.x() < 0 && line.x() > 0)) {
					closest_point = cBegin;
				}
				else {
					auto line1 = line;
					line1.normalize();
					closest_point = cBegin + line1 * proj.length();
				}
			}
			if ((closest_point - position).length() < mag_range) {
				auto f = (closest_point - position);
				auto l = f.length();
				// wow amazing hack v2.0
				double k = 1000 * l*exp(-l * l * 5000);
				f.normalize();
				f *= k;
				force += f;
			}
		}



		/////////////////////////////////////////////////////////////////////
		// APPLY FORCES
		/////////////////////////////////////////////////////////////////////

		// send computed force, torque, and gripper force to haptic device
		hapticDevice->setForceAndTorqueAndGripperForce(force, torque, gripperForce);

		// signal frequency counter
		freqCounterHaptics.signal(1);
	}

	// exit haptics thread
	simulationFinished = true;
}

//------------------------------------------------------------------------------
